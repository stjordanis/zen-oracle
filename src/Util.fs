module Util

open System
open Logary.Message
open Infrastructure
open Result
open FSharp.Data
open Config
open Serialization

let result = new ResultBuilder<string>()

let getUri action =
    sprintf "http://%s/%s" config.yaml.nodeApi action
    
let getResponseBody request errorMessage = result {
    let! response = 
        Exception.resultWrap<HttpResponse> request errorMessage
    
    if response.StatusCode <> 200 then 
        return! Error (sprintf "%s: %d" errorMessage response.StatusCode)
            
    return 
        match response.Body with
        | Text text -> text
        | Binary bytes -> String.deserialize bytes
}

let chain =
    match config.yaml.chain with
    | "main" -> Consensus.Chain.Main
    | "test" -> Consensus.Chain.Test
    | _ -> Consensus.Chain.Local

let (|UInt64|_|) str =
   match UInt64.TryParse(str) with
   | (true, i) -> Some i
   | _ -> None